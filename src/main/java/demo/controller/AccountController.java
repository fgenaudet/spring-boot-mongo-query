package demo.controller;

import demo.bean.Account;
import demo.bean.AccountEvent;
import demo.repository.AccountEventRepository;
import demo.repository.AccountRepository;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountEventRepository accounteventRepository;

    @RequestMapping(method = RequestMethod.POST)
    public List<Account> accountsByName(@RequestBody String name) {
        return accountRepository.findByName(name);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/events")
    public List<AccountEvent> accountEventsByAccountName(@RequestBody(required = false) String name) {
        if (name != null)
            return accounteventRepository.findByAccountName(name);
        else
            return accounteventRepository.findAll();
    }


    @RequestMapping(method = RequestMethod.POST, value = "/events-id")
    public List<AccountEvent> accountEventsByAccountName(@RequestBody(required = false) BigInteger id) {
        if (id != null)
            return accounteventRepository.findByAccountId(new ObjectId(id.toString(16)));
        else
            return accounteventRepository.findAll();
    }
}
