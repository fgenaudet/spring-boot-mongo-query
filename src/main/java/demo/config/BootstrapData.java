package demo.config;

import demo.bean.Account;
import demo.bean.AccountEvent;
import demo.repository.AccountEventRepository;
import demo.repository.AccountRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class BootstrapData implements InitializingBean {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountEventRepository accountEventRepository;

    @Override
    public void afterPropertiesSet() throws Exception {
        //Delete all accounts
        accountRepository.deleteAll();

        // Add accounts
        accountRepository.save(new Account().setName("COUCOU 0"));

        Account account = new Account().setName("COUCOU 1");
        accountRepository.save(account);

        // Delete all account events
        accountEventRepository.deleteAll();

        // Adds account events
        accountEventRepository.save(new AccountEvent().setAccount(account).setName("Event 1").setDate(new Date()));
        accountEventRepository.save(new AccountEvent().setAccount(account).setName("Event 2").setDate(new Date()));
    }
}
