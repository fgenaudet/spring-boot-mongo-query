package demo.repository;

import demo.bean.AccountEvent;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface AccountEventRepository extends MongoRepository<AccountEvent, Long> {
    @Query("{'account.name': ?0}")
    List<AccountEvent> findByAccountName(String name);

    @Query("{'account.$id': ?0}")
    List<AccountEvent> findByAccountId(ObjectId objectId);
}
