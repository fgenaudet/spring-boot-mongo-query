package demo.repository;

import demo.bean.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface AccountRepository extends MongoRepository<Account, Long> {
    @Query("{name: ?0}")
    List<Account> findByName(String name);
}
