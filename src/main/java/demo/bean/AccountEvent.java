package demo.bean;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigInteger;
import java.util.Date;

@Document
public class AccountEvent {
    @Id
    private BigInteger id;

    @DBRef
    private Account account;

    private String name;
    private Date date;

    public BigInteger getId() {
        return id;
    }

    public AccountEvent setId(BigInteger id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public AccountEvent setName(String name) {
        this.name = name;
        return this;
    }

    public Date getDate() {
        return date;
    }

    public AccountEvent setDate(Date date) {
        this.date = date;
        return this;
    }

    public Account getAccount() {
        return account;
    }

    public AccountEvent setAccount(Account account) {
        this.account = account;
        return this;
    }
}
